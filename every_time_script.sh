#!/bin/bash



# 啟動後端 (確認先前已佈署過了才需要)
docker exec node-server bash -c 'pm2 start apps/express-ts/pm2.json'

# 容器給予 /var/run/docker.sock 權限 (測試Playwright才需要)
docker exec -u root my-jenkins bash -c 'chown jenkins:jenkins /var/run/docker.sock && chmod 660 /var/run/docker.sock'