#!/bin/bash

# 此腳本執行條件:
#   容器第一次啟動時執行



# 安裝 pm2, 加入 .bash_profile
docker exec -u root node-server bash -c 'npm i -g pm2'

# 安裝 bun 加入 .bash_profile
docker exec -u node node-server bash -c 'curl -fsSL https://bun.sh/install | bash'
docker exec -u node node-server bash -c 'echo -e "# bun\nexport BUN_INSTALL="\$HOME/.bun"\nexport PATH=\$BUN_INSTALL/bin:\$PATH" >> ~/.bash_profile'


# 安裝 bun 加入 .bash_profile
docker exec my-jenkins bash -c 'curl -fsSL https://bun.sh/install | bash'
docker exec my-jenkins bash -c 'echo -e "# bun\nexport BUN_INSTALL="\$HOME/.bun"\nexport PATH=\$BUN_INSTALL/bin:\$PATH" >> ~/.bash_profile'

# 容器給予 /var/run/docker.sock 權限
docker exec -u root my-jenkins bash -c 'chown jenkins:jenkins /var/run/docker.sock && chmod 660 /var/run/docker.sock'