https://hub.docker.com/

Slim 鏡像是完整鏡像的精簡版本

Alpine 鏡像基於 Alpine Linux 項目，該項目是專門為容器內部使用而構建的操作系統。很長一段時間以來，這些是最受歡迎的鏡像變體，因為它們的體積非常小。

Debian 版本號與名稱對應
Forky: 14
Trixie: 13
Bookworm: 12
Bullseye: 11
Buster: 10
Stretch: 9
Jessie: 8